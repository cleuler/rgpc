---
title: "GPC: Noções Básicas de R - Aula 1"
author: "Prof. Dr. Cleuler Barbosa das Neves"
date: "Goiânia-GO, `r format(Sys.Date(), '%d/%B/%Y')`"
in_header: \usepackage{graphicx, color, verbatim, fancyvrb, setspace, amsfonts, amsmath, amssymb, pandoc}
output:
  html_document: default
  pdf_document: default
  word_document: default
header-includes:
 - \usepackage{fvextra}
 - \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}
fig_caption: yes
always_allow_html: yes # permite a geração de tabelas só em html sem replicá-las em pdf
---
Exemplo de uso de R + Markdown + knitr
======================================

Prof. Dr. Cleuler Barbosa das Neves  
[currículo.lattes](http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4786159E2)  
  
  *Built with R version n. `r getRversion()`*  

# O que é R?

R é um ambiente programático voltado para importação e tratamento de dados visando a computação de métodos estatatísticos com a geração de gráficos.  

> _R is an integrated suite of software facilities for data manipulation, calculation and graphical display [...]._ (VENABLES; SMITH; R CORE TEAM, 2018)  

Derivado do software S, com que John Chambers venceu o *ACM Software Systems Award*.  
Uma desvantagem é que o uso do R pode requerer transformações nos dados iniciais para viabilizar um modo para analisar, visualizar e manipular dados.  
R é baseado em linhas de comandos e foi desenvolvido em Linguagem C.  
O R base tem menos pacotes estatatísticos inclusos em comparação com SPSS ou SAS, mas é muito mais fácil extendê-los.  

Atulamente, existem disponiveis no website CRAN - _Comprehensive R Archive Network_ aproximadamente 13.700  pacotes [13719 available packages] (CRAN, 2019)  (https://cran.r-project.org/)  
Para *baixar* um desses **packages** basta chamar, uma só vez, a função ``install.packages()``, indicando seu argumento entre áspas; depois será preciso carregar uma das funções desse pacote na *Global Enviroment* através das funções ``library()`` ou ``require()``, indicando o nome da função como argumento.  

***
  
Os interessados em um excelente **curso de R *on line* ** e em obter **Material para cursos de R** devem [clicar aqui](http://material.curso-r.com/)  
  
Os interessados em compreender o pacote **Knitr**: *a general-purpose package for dynamic report generation in R* -- R package version 1.5 devem [clicar aqui](http://yihui.name/knitr)  
  
Os interessados em compreender como escrever uma fórmula matemática dentro do **RMarkdown** devem [clicar aqui](http://www.statpower.net/Content/310/R%20Stuff/SampleMarkdown.html)  
  
Os interessados em tirar dúvidas sobre como gerar relatórios dinâmicos .PDF pelo uso do Latex combinado com Rmarkdown que use fórmulas matemáticas com qualidade na sua edição devem consultar o LaTeX/Mathematics [clicar aqui](https://en.m.wikibooks.org/wiki/LaTeX/Mathematics)  
Os interessados em aprender sobre como utilizar o operador **pipe %>%** do pacote *magrittr*  devem [clicar aqui](https://cran.r-project.org/web/packages/magrittr/vignettes/magrittr.html)  

Os interessados em assinar a **Lista Brasileira do R** -- [R-br] da **UFPR** devem [clicar aqui](http://listas.inf.ufpr.br/cgi-bin/mailman/listinfo/r-br)  
  
***
  
# R como calculadora
  
  R retorna na saída (*console area*) um objeto vetor.  
  Por isso cada saída é precedida de [], que representa todos os elementos desse vetor anônimo  
```{r calculadora}

15 + 5

15 - 5

15 * 5

15 / 5

15 ^ 4

sqrt(50625) # é a função raiz quadrada, abreviatura de *square root*

1 / 0

sqrt(-1)

x <- 3 # atribui o valor inteiro 3 à variável denominada pela letra x.

y <- 2

x + y # retorna como resultado a soma dos valores das variáveis x e y.

pi
round(pi, 4) # a função round() arredonda seu primeiro argumento, adotado como número de casas decimais o segundo argumento passado; se este número de casas não é repassado assume como default o valor zero.
args(round) # a função args() retorna os argumentos da função que lhe é repassada.
round(pi)

y <- 15
x <- 5
x - y
ls() # ls (abreviaura de *list*) é o nome de uma função, que retorna uma lista de *characters*, em ordem alfabética, que correspondem aos nomes das variáveis que se encontram no *Global Enviroment*.
args(ls) # A função args() retorna os argumentos da função cujo nome lhe é repassado; observe que o R admite a composição de duas ou mais funções.
rm(list = ls()) # A função rm(), abreviatura de *remove*, remove toda a *list* de variáveis da *Job Area* que lhe é repassada pela função ls(), isto é, dá um reset no *Global Environment*.
args(rm) # Retorna os argumentos da função rm().

library(gdata) # Carrega para o *Global Environment* o *package* denominado gdata.
Args(rm) # Chama a função Args(), que é diferente da função args(), pois a linguagem R é *case sensitive*; aquela retorna, na forma de uma tabela, os nomes dos argumentos de uma função e suas características (tipos).
```

#Acessando e definindo funções  

##Função function()

A função ``function()``  é uma das principais funções do R;  
Ela traz em si a base em que a própria Linguagem R encontra-se entruturada.
Uma **função**, uma vez definida, pode receber **argumentos** e pode retornar resultados ou **valores** através da função *return(value)*.  
Principais *argumentos* de uma função podem ser obtidos pela função: args(nomeDaFuncao).  

Exemplo de definição e uso de uma função.  
Q1) Como elaborar uma função em R para obter o resultado da série: somatório x${_{i}}$, para i = 1 à n, n inteiro diferente de zero. 

${\displaystyle \sum _{i=1}^{n}x_{i}=x_{1}+x_{2}+\cdots +x_{n}}$  

Resposta
```{r somaComExemplos}
# Início da função somatoria
#---------------------------
# Função para retornar a soma de todos os elementos de um vetor numérico
# Argumento de entrada será denominado de x; espera-se um <vctr> <num>
somatoria <- function(x){
  n <- length(x) # retorna o cumprimento do vetor x; o argumento da função.
  soma <- 0
  for (i in 1:n) {
    soma <- soma + x[i]
  }
  return(soma)
} # Fim da função somatoria

Somatoria <- function(x){
  soma <- 0
  for (xi in x) {
    soma <- soma + xi
  }
  return(soma)
} # Fim da função Somatoria
#--------------------------

args(somatoria)
#somatoria() # Retornará um erro, porque nenhum argumento foi repassado.
# Chamando a função somatória e passando-lhe uma <var> <vector> <num> como seu argumento, como é esperado pela função, assim como foi concebida:
a <- 1
somatoria(a)
Somatoria(a)

a <- c(1, 2, 3) # c é a função *combine*, que retorna um vetor formado pelos argumentos que lhe forem passados (devem ser separados por vírgula)
a
somatoria(a)
Somatoria(a)


a <- c(1.5, 2, 3.5) # Os argumentos da função c() podem ser *interger* ou *float* ou *character*
a
somatoria(a)
Somatoria(a)
somatoria(a) == Somatoria(a) # Mas essas duas funções não são identicas, porque apresentam corpos diferentes(secripts ou códigos diferentes); embora produzam o mesmo resultado.

a <- c(1:4) # Os dois pontos no R geram sequências do primeiro inteiro até o último, de um em um, valendo-se dos dois argumentos passados.
a
somatoria(a)
a <- c(1:100)
a
somatoria(a)
a <- c(1:1000)
a
somatoria(a)
Somatoria(a)
sum(a)
args(sum) # A função args() retorna os argumentos da função que lhe é repassada.
a[1001] <- NA # um valor correspondente à *Not Avaliable* é atribuído à posição número 1001 do vetor numérico denominado pela letra a.
a # este comando exibe no console o vetor a.
somatoria(a) # Retorna o valor NA, porque basta uma NA para que a soma de várias parcelas diferente de NA resulte em um valor NA.
Somatoria(a)
sum(a)
sum(a, na.rm = TRUE) # O valor NA da posição 1001 do vetor a não será considerado para não se indeterminar a soma das outras 1000 parcelas do mesmo vetor a.
mean(a, na.rm = TRUE)# nem de sua média
sd(a)
sd(a, na.rm = TRUE)  # nem de seu desvio padrão (*standart desviation*)
```

 Refletir e responder às seguintes questões *pragmáticas*:  
 1) Olhando para o código (*script*) da função somatoria quais regras sintáticas (estruturais) pode-se extrair do uso dos parenteses, das chaves e dos colchetes na Linguagem R?  
 2) Idem para o uso da vírgula (,) e do símbolo de igual (=)?  
 3) Diferencie: objetos, comandos, funções, símbolos e operadores da Linguagem R.  Exemplifique.  
 4) A expressão =!=== é uma *wff* (*well formuled formula*)? Ela faz sentido em Linguagem R? Ou nalguma outra?  
 5) Se faz, qual o *significado* desse conjunto de *significantes*? Se não, qual rearranjo desses *símbolos* seria capaz de conferir-lhe algum *sentido* em Linguagem R? Quiçá aquele que teria sido implicitamente desejado?  
 6) Olhando para o mesmo *script* das funções somatoria e Somatoria extraia e formule outras regras sintáticas que estruturam o uso da Linguagem R.  
 7) Elabore um código para a função identidade na reta. Gere  um gráfico para essa função no intervalo [0,5].  
 8) Redija e salve um script para a função linear em **R**. Gere  um gráfico para essa função no intervalo [0,5] e salve-o no formato .pdf.  
 9) Apresente duas funções lineraes que sejam inversas. Plote-as juntamente com a função identidade.  
 10) Descrever os tipos de variáveis geradas na *Global Environment Area* pelo seu script e suas características: nome, tipo e atributos.
   
  **Trabalho Final do curso:  **
  Desde a primeira aula é preciso iniciar a concepção de um projeto para uma prova de conceito.  
  Produzir um script capaz de carregar dados, tratar esses dados, processar os dados adequadamente tratados e exibir os resultados desse processamento na forma de tabelas, de gráficos e de relatórios dinâmicos, tudo de modo a antender a uma das necessidades de cálculo da GPC - Gerência de Precatórios e Cálculos da PGE-GO.  
  Ãqueles que não integrarem essa Gerência, deverão coneceber uma prova de conceito em suas respectivas áreas de interesse proficional.
  
  
# Com a perna no Mundo...As funções... e seus pacotes!
# [=====================================]
# [A 1ª faz CRAN! A 2ª faz CRAN! E CRAN, CRAN, CRAN!!!]
# [=====================================]

```{r bibliotecas}
#[=========================================================================]
#[                   Pacotes do System Library                             ]
#[=========================================================================]

# Pacotes de importação de BD
# para ativar um pacote do System Library (vem c/a instalação do R): 2.000 f's
library(datasets) # esse argumento não precisa das aspas
# *This package contains a variety of datasets. For a complete list, use library(help = "datasets").*
#

#[=========================================================================]
#[                    Pacotes da User Library                              ]
#[=========================================================================]

#P/instalar um pacote da web (CRAN) basta executar install.packages() 1 vez
library(foreign) # esse argumento não precisa das aspas
# Para carregar Base de Dados dos aplicativos:
# Minitab, S, SAS, SPSS, Stata, Systat, Weka, dBase ...

#install.packages("data.table") # Para carregar BD's de grandes dimensões
library(data.table) # (p.53-53 do livro R_CS); argumento não precisa das aspas
# 1- converter o arquivo para .csv usando a função fwf2csv () do pacote descr
# 2- carregar a BD com a função fread() do pacote data.table, que usa menos
#    memória que a função read.fwf() do pacote ...
#install.packages("sqldf") # p/carregar partes de BD's de grandes dimensões
library(sqldf) # R_SC: (p. 53-54)

#install.packages("descr") # Um pacote tem de ser instalado 1 vez no seu micro
library(descr) # Ativa o pacote; então suas funções são disponibilizadas p/uso
# "descr" é um pacote com funções voltadas para Estatística Descritiva

#install.packages("gdata")
library(gdata) # pacote para manipulação de dados (BD's) (p. 45)
               # No Windows poderá ser necessário instalar ActivePerl
               # ou outro interpretador da linguagem perl.

#install.packages("igraph") # Océu é o limite!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
library(igraph) # pacote para Network Analysis and Visualization
                # R_CS: cap. 12- Análise de Redes Sociais (com grafos)

#install.packages("knitr")
library(knitr) # pacote para geração de Relatório Dinâmico em R (p. 119)

#install.packages("memisc") # para surveys
library(memisc) # pacote para manipulação e pesquisa de dados (p. 66, 89)
                # e para apresentação de análises de seus resultados

#install.packages("rgdal") # para exibição de Mapas e dados espacializados
library(rgdal) # R_SC: cap. 11- Mapas (p. 134-139)
# Requer a instalação do pacote sp
#install.packages("sp")
library(sp)

#install.packages("rmarkdown") # para instalação do RMarkdown
library(rmarkdown) # R_SC: geração Relatórios Dinâmicos no RStudio(p. 115-19)
# Requer instalação de outros pacotes p/rodar o RMarkdown dentro do RStudio
#install.packages("htmltools") - esse não precisou, veio c/o RMarkdown
library(htmltools) # Ferramentas para HTML
#install.packages("caTools")#   - esse precisou e instalou o bitops
library(caTools) # Tools: moving windows statistics, GIF, Base64, ROC AUC etc.

#install.packages(c("bindr", "bindrcpp", "Rcpp", "stringi"))
library(bindr)# library deve ter package com comprimento 1
library(bindrcpp)#
library(Rcpp)#
library(stringi)#
#install.packages(c("cluster", "Matrix"), lib="C:/Users/cleuler-bn/Documents/R/R-3.4.4/library")
library(cluster)#
library(Matrix)#

#install.packages(c("financial", "FinancialInstrument", "FinancialMath"))
#library(financial)#
library(FinancialInstrument)#
library(FinancialMath)#

#install.packages("pander") # Prints an R object in Pandoc's markdown.
library(pander)

#install.packages("flextable")
library(flextable)

#install.packages("tinytex")#   - foi preciso instalar para gerar arquivo .pdf direto do RMarkdown
#library(tinytex)# para carregar o pacote tinytex, que gera arquivo .tex e certamente converte para .pdf
# Mas isso gerou uma v2.pdf no formato de uma janela do PDF, sem os marcadores do lado esquerdo!!!!!
# Do Jeito antigo estava melhor e gravava um .pdf na pasta R_CS/Aula1 que ao abrir no Adobe
# Apresentou na parte esquerda da tela do Adobe todos os marcadores das secções do arquivo (melhor)!
#[=========================================================================]
#[                                                                         ]
#[=========================================================================]
```
# help
```{r socorro}
help() # Pode-se chamar a função help() substituíndo-a por ?
help.start()
args(setwd) # Retorna os argumentos da função setwd(), que aponta para um dado diretório como sendo o diretório de trabalho; é a abreviatura de *set work directory*.
help(demo)
?q
args(apropos)
?apropos
apropos("csv") # Qual o tipo do seu arg? É diferente do argumento de args?
args(help.search)
?help.search
help.search("install.packages") # Exibe umlink para o help do nome da função passada.
??install.packages
??network
RSiteSearch("social network analysis") # Abre o R site search no navegador
example("ls") # O que significa a abreviatura ls?
??demo
demo("graphics")# Exibe demonstrativamente alguns tipos de gráficos que se pode gerar.
```

# Objetos

# Vetores

Conjunto de elementos do mesmo tipo (*logical, numeric, integer, double, character, factor*)

1. A forma mais simples de se criar um vetor é chamar a usual função *combine*: ``c()``.  

```{r}
valor.numerico  <- c(3, 4, 2, 6, 20)
valor.numerico
str(valor.numerico)

valor.char <- c("koala", "kangaroo")
valor.char
str(valor.char)

valor.logical <- c(FALSE, FALSE, TRUE, TRUE)
valor.logical
str(valor.logical)
```

2. Segunda maneira de criar um vetor no R: usando a  função ``scan``

```{r}
valores <- scan(text = "
                       1
                       2
                       3
                       4
                       5"
                )

valores
str(valores)
is.integer(valores) # houve uma coerção automática de valoes <char> para <dbl>

names(valores) <- letters[1:5] # atribuindo nomes para todas as posições do vetor criado
valores
str(valores)
typeof(valores)
class(valores)
```

3. Outra opção usando comando ``rep``

```{r}
rep(1, 5)

rep(c(1, 2), 3)

rep(c(1, 6), each = 3)

rep(c(1, 6), c(3, 5))

rep(c(1, 2), times = 3, each = 2)
```

4. Outra opção usando comando ``seq``

```{r}
seq(from = 1, to = 5)

seq(from = 1, to = 5, by = 0.1)

seq(from = 1, to = 5, length = 10)

rep(seq(from = 1, to = 5, length = 10), each = 3)
```

5. Outra opção usando o operador ``:``

```{r}
1:5

c(1:5, 10) # a função c() faz coerções automáticas quando diante de tipos diferentes
str(c(1:5, 10))
str(1:5)
```

## Operações com Vetores

1. soma, multiplicação, divisão 
```{r operacoesComVetores}
x <- 1:4

y <- 5:8

x + y

2 * x + 1

2 * (x + 1)

x * y

x / y # Os operadores fazem coerções automáticas de tipos para adaptar seus resultados
str(x)
str(y)
str(x * y)
str(x / y)
```

2. aplicando funções matemáticas em vetores

```{r}
log(x)

log(x, 10)

sum(x)

mean(x)

prod(x) # fornece o produto   dos elementos do vetor x.

var(x)  # fornece a variância dos elementos do vetor x.
```

3. aplicando a **Regra da Reciclagem** em operações com vetores de diferentes tamanhos

```{r}
# IMPORTANTE: Regra da Reciclagem
z <- 1:2
x * z

w <- 1:3
x * w
```

# Matrizes

Conjunto de elementos dispostos em linhas e colunas, em que todos os elementos são do mesmo tipo, ou seja, todos numéricos ou todos *characteres*.

1. criando matrizes de diferentes tamanhos e de direrentes tipos

```{r matrizes}
mat.num <- matrix(c(1:16), 4, 4)
mat.num
str(mat.num)

mat.char <- matrix(LETTERS[1:4], 2, 2)
mat.char
str(mat.char)
```

2. manipulando atributos de uma matriz

```{r manipulandoMatrizes}
# Atributos de um Objeto: criando nomes para as linhas de uma matriz
rownames(mat.num) <- c("Sao Paulo", "Americana", "Piracicaba", "Madson")
colnames(mat.num) <- 1:4
mat.num
```

3. multiplicação de duas matrizes: elemento a elemento ou multiplicação algébrica

```{r}
# Multiplicação elemento a elemento
mat.num2 <- diag(seq(10, 40, by = 10))
mat.num2

mat.num3 <- mat.num * mat.num2 # mat.num3 herda os atributos de mat.num: colnames e rownames
mat.num3

# Multiplicação algébrica de Matrizes
identidade <- diag(4)
identidade
mat.num %*% identidade # toda matriz algebricamente multiplicada por uma matriz identidade resulta nela mesma.
```

4. acessando elementos de matrizes

```{r}
mat.num
# Um elemento
mat.num[1, 1]

# Uma Linha; no caso a primeira linha (com todas as suas colunas)
mat.num[1, ]

# Uma Coluna; no caso a terceira coluna (com todas as suas linhas)
mat.num[, 3]

# Sub Matrizes
mat.num[c(1, 3, 4), c(2, 1, 4)]
mat.num[c(T, F, T, T), c(T, T, F, T)]
mat.num[2, 3]

# Índices inteiros negativos representam os elementos que não serão acessados
mat.num[-2, -3]
mat.num[-c(1, 3, 4), -c(2, 1, 4)]
```

# Data.frames

1. São similares às matrizes, no entanto permitem que as colunas contenham variáveis de diferentes tipos: numérico; character; factor etc.
  
```{r dataFrames}

data(iris) # Carrega o Banco de Dados denominado iris, que vem com o pacote básico do R

iris

iris$Sepal.Length

# Acrescentando uma variável <lgl> ao data frame carregado:
iris$Petal.Teste <- iris$Petal.Length >= 5.5 & iris$Petal.Width >= 2.0
iris
```

2. Utilizando o operador pipe %>% para obter um código mais legível e inteligível.

```{r}
library(dplyr)
library(knitr)
# Observe-se que a variavél iris$Species é do tipo _factor_ <fctr>
knitr::kable(iris, caption = "Iris data frame")

iris %>% 
  subset(Petal.Teste == TRUE) %>% 
  select(Species, Petal.Teste)

iris %>% 
  subset(Petal.Teste == TRUE) %>% 
  select(Species, Petal.Teste) %>% 
  nrow %>% 
  cat("é o número de observações que satisfazem a condição: comprimento da pétala >= 5.5 e peso da pétala >= 2.0\n")

iris %>% 
  subset(Petal.Teste == TRUE) %>% 
  select(Species, Petal.Teste) %>% 
  table %>% 
  cat("é o número de observações, somente na terceira espécice (virginica), que satisfazem a condição: comprimento da pétala >= 5.5 e peso da pétala >= 2.0\n")
```

3. Exibindo uma tabela mais elegante com o operador pipe %>% e a função pander()  
  
Os interessados no package flextable (só funcionou para gerar arquivos html) devem [clicar aqui](https://davidgohel.github.io/flextable/articles/overview.html)  

```{r}
library(magrittr)
library(pander)
#library(flextable) # Essa função deste pacote dá um erro quando gerando a tabela no latex, o que impede a exportação de um pdf deste Rmarkdown
iris %>% 
  subset(Petal.Teste == TRUE) %>%
  select(Petal.Length, Petal.Width, Species, Petal.Teste) %>%
  pander(caption = "Subset of Iris data frame")
```

```{r gerar_tabelaPdfRmarkdown}
#--------------------------------
# Início da função minhaTabelaPdf
minhaTabelaPdf <- function(dataObject, tituloTabela = "") {
  library(pander)
  pander(dataObject, caption = tituloTabela)
} #  fim da função minhaTabelaPdf
#--------------------------------
```

```{r}
library(magrittr)
iris %>% 
  subset(Petal.Teste == TRUE) %>%
  select(Petal.Length, Petal.Width, Species, Petal.Teste) %>%
  minhaTabelaPdf(tituloTabela = "Subset of Iris data frame")
```
  
4. Exibindo uma tabela mais sofisticada com o operador pipe %>% e a função datatable()  
  
Os interessados nos __packages DT, shiny, datasets__ (só funcionam para gerar arquivos html) devem [clicar aqui-1](https://github.com/rstudio/DT/issues/31),  
[clicar aqui-2](https://datatables.net/reference/option/) e também  
[clicar aqui-3](https://datatables.net/manual/styling/classes).  
Clicar [aqui-4](https://rstudio.github.io/DT) _for the full **DT's** documentation._  

```{r}
library(magrittr)
if(!library(DT, logical.return = TRUE)) install.packages("DT")
library(DT)
subSetIris <- iris %>%
                subset(Petal.Teste == TRUE) %>%
                select(Petal.Length, Petal.Width, Species, Petal.Teste)
datatable(subSetIris, caption = "Subset of Iris data frame") %>% 
  formatStyle(columns = "Petal.Length", target = "cell", backgroundColor = "#F7080880") %>% 
  formatStyle(columns = "Petal.Width" , target = "cell", backgroundColor = "#F7080820") %>% 
  formatStyle(columns = "Species"     , target = "cell", backgroundColor = "#F7080880") %>%
  formatStyle(columns = "Petal.Teste" , target = "cell", backgroundColor = "#F7080820")
    
```

# List

1. Generalização de vetores no sentido de que uma lista é uma coleção de objetos
  
```{r listas}
# Exemplo de uma lista com os quatro tipos básicos que um vetor pode ser
x <- list(1:3, "a", c(TRUE, FALSE, TRUE), c(2.3, 5.9))
x
str(x)
```

2. Exemplo de uma list criada atribuindo nomes aos seus elementos e que contem vetores, matrizes e outras lists como seus elemento; no caso são todos do tipo _numeric_

```{r}
sequencia <- c(rep(1:4, 2, each = 2))
A <- list(x = 1:4,
         y = matrix(1:4, 2, 2),
         w = sequencia,
         v = list(B = 4,
                  C = 5))
A

A[[1]]
A[[4]]

A$x
A$y
A$w
A$v

str(A)
```

3. Criando uma list com elementos numéricos: 5 _intergers_ na primeira posição e 2 _double_ na segunda

```{r}
B <- list(r = 1:5, s = 2)
str(B)
```

4. Uma estrutura de dados tipo list pode ser recursiva, ou seja, pode conter elementos que sejam do tipo list. Essa sua principal diferença para o tipo vetor, que não pode.

```{r}
Q = c(A, B) # Como os dois elementos são lists a função c() faz uma concatenação delas, gerando uma lista de 6 posições: quatro da primeira mais duas da segunda

Q
str(Q)
```

