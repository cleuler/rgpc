---
title: "GPC: Noções Básicas de R - Aula 6.0"
author: "Prof. Dr. Cleuler Barbosa das Neves"
date: "Goiania-GO, `r format(Sys.Date(), '%d de %B de %Y')`"
in_header: \usepackage{graphicx, color, verbatim, fancyvrb, setspace, amsfonts, amsmath, amssymb, pandoc, fontenc, fontch}
output:
  html_document: default
  pdf_document: default
  word_document: default
header-includes:
 - \usepackage{fvextra}
 - \DefineVerbatimEnvironment{Highlighting}{Verbatim}{breaklines,commandchars=\\\{\}}
fig_caption: yes
always_allow_html: yes # permite a geração de tabelas só em html sem replicá-las em pdf
---
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```
[currículo.lattes](http://buscatextual.cnpq.br/buscatextual/visualizacv.do?id=K4786159E2)  


## R - Resolução da Lista de Exercícios sobre séries temporais


### Uma análise do clima de Goiânia nos anos de 1985-2018 (34 anos)
1. Construa um _data frame_ com dados de Goiânia para os anos de 1985-2018 (34 anos)   
1ª coluna – campo tipo _date_ (dia-mês-ano)  
2ª coluna – temperatura média diária em Goiânia (buscar fontes oficiais de dados na web)   
Ex.: [BDMEP - Banco de Dados Meteorológicos para Ensino e Pesquisa](http://www.inmet.gov.br/portal/index.php?r=bdmep/bdmep) do INMET - Instituto Nacional de Meteorologia  
  
2. Amplie o dataframe para que:  
3ª coluna – média pluviometrica diária em Goiânia (mm/m${^{2}}$)  
  
3. Exiba um gráfico da curva de temperatura e outro dos índices de pluviometria de Goiânia para toda a série temporal e para o ano de 2018.  
Interprete-os.  
  
4. Qual a temperatura e a pluviometria média de Goiânia em 2018 e para toda a série temporal?  
Qual o desvio-padrão (_s_) dessas duas medidas de tendência central?  
Exiba mais essas duas informações nos dois gráficos anteriores (exercício n. 3).  
Descreva o comportamento sazonal do clima de Goiânia no ano de 2018 e para toda a série temporal.  
  
5. Há _outliers_ nesses conjuntos de dados (no ano de 2018 e em toda série temporal)?  
Qual o valor da media (${\displaystyle \mu}$) e do desvio-padrão (_s_ da amostra ou ${\displaystyle \sigma}$ da população) se esses _outliers_ forem desconsiderados?  
  

  
## Solução Dos Exercícios N. 1 e 2

```{r operator_pastesTogetherStrings, include = TRUE, echo = TRUE}
`%+%` <- function(a, b) paste(a, b, sep = "")
```

```{r solucaoDoExercicioN1e2}
#---------------------------------------------------------------------------------------------------------------
#1. Construa um _data frame_ com dados de Goiânia para os anos de 1985-2018 (34 anos):  
# 1ª coluna – campo tipo _date_ (dia-mês-ano)  
# 2ª coluna – temperatura média diária em Goiânia (buscar fontes oficiais de dados na web)   
# Ex.: [BDMEP - Banco de Dados Meteorológicos para Ensino e Pesquisa](http://www.inmet.gov.br/portal/index.php?r=bdmep/bdmep) do INMET - Instituto Nacional de Meteorologia  
  
#2. Amplie o dataframe para que:  
# 3ª coluna – média pluviometrica diária em Goiânia (mm/m${^{2}}$)  
##--------------------------------------------------------------------------------------------------------------- 
# Função para importar uma tabela .csv com a série temporal da temperatura média, mín., máx. e pluviometria de Goiânia no período 1984-2018 (34 anos)
lerSerieGyn.csv <- function(nome.arquivo = "GynTemperaturaPluviometriaDados_1985-2018.csv", pular = 16) {
  read.csv2(file = nome.arquivo,
            skip = pular,
            header = TRUE,
            sep    = ";",
            quote  = "\"",
            dec    = ".",
            fill   = TRUE,
            stringsAsFactors = FALSE,
            fileEncoding = "UTF-8")
}
# Testando a função lerSerieGyn.csv
GynClima <- lerSerieGyn.csv(nome.arquivo = "../Entrada/GynDados_1985-2018Alter.csv")
```

```{r TratarDados}
#[================================================================]
#[           Tratar os Dados e suas inconsistências               ]
#[================================================================]
# Verificando os tipos das variáveis do arquivo carregado
GynClima
# Talvez a variável Estacao, importada como character <chr>, deva ser transformada em variável categórica, denominada de factor <fctr>.
```

```{r TransformarDados}
#[================================================================]
#[                     Transformar os Dados                       ]
#[================================================================]
# O 3 dataframe: GynClima.
# Tranformar variável <var> Estacao, que foi lida como <int>, do tipo "integer", para variável categórica, tipo factor <factr>:
GynClima$Estacao <- as.factor(GynClima$Estacao)

# Tranformar variável <var> Data, que foi lida como <chr>, do tipo "character", para variável date, tipo date <date>:
GynClima$Data <- as.Date(GynClima$Data, format = "%m/%d/%Y")


# Eliminando a 3ª coluna, correspondente à variável <var> Hora, tipo <int>
# GynClima <- GynClima[, -3]

# Capturar os valores das linhas pares das <var> Precipitacao e TempMin e copiar nas linhas ímpares 
# GynClima$Precipitacao[seq(from = 1, to = nrow(GynClima) - 1, by = 2)] <- GynClima$Precipitacao[seq(from = 2, to = nrow(GynClima), by = 2)]
# GynClima$TempMin[seq(from = 1, to = nrow(GynClima) - 1, by = 2)] <- GynClima$TempMin[seq(from = 2, to = nrow(GynClima), by = 2)]

##########
# ERRO 1 #
##########
# Estava dando um erro porque os dados para a data de 17/10/2015 só apresentam os dados de 12:00 h;
# Mas não o de 0:00 h
# Repetiu-se os dados de 0:00 h do dia 16/10/2015 para zero hora do dia 17/10/2015 no tabela .csv de entrada.
# Porque senão os dados de NA seriam replicados a partir dessa data 17/10/2015, por falta de uma linha.
# O melhor é tomar essas observaçõe como NA's, ou seja, não registradas
GynClima[GynClima$Data == as.Date("2015-10-17") & GynClima$Hora == 0, 4:7] <- NA
GynClima[GynClima$Data == as.Date("2015-10-17") & GynClima$Hora == 0, ]
```


```{r TransformarDados2}
# É preciso identificar todas essas 118 datas não duplicadas.
# Depois inserir NAs nos respectivso dados nã coletados: sejam eles a 0 h ouàs 1200 h

# Localizando as medidas (observações) que não foram coletadas a 0 h e também às 1200 h
NaoDuplicados <- GynClima$Data[ !(GynClima$Data %in% GynClima$Data[duplicated(GynClima$Data)]) ]
NaoDuplicados
index <- vector(mode = "integer", length = length(NaoDuplicados))
for(i in 1:length(NaoDuplicados)) {
  print(GynClima[GynClima$Data == as.Date(NaoDuplicados[i]), ])
  index[i] <- rownames(GynClima[GynClima$Data == as.Date(NaoDuplicados[i]), ])
} # As 114 datas não duplicadas correspondem a registros que só foram realizados às 12:00 h
#   Mais 4 que corresponde a registros que só foram realizados às 0 h:
# "1994-02-12", a de núm. 6635
# "1994-05-13", a de núm. 6814
# "1994-07-25", a de núm. 6959
# "1994-12-05", a de núm. 7223

GynClima[index, ] # exibindo as 118 obs. com um só dos registros.
n <- as.integer(index) # transformando os index tipo char em interger
novaLinha <- data.frame(Estacao       = GynClima$Estacao[1],
                        Data          = as.Date(NaoDuplicados),
                        Hora          = (GynClima$Hora[n] == 0) * 1200L,
                        Precipitacao  = NA,
                        TempMax       = NA,
                        TempMin       = NA,
                        TempCompMedia = NA)
row.names(novaLinha) <- index

library(DataCombine)
for (j in seq_along(NaoDuplicados)) {
  if(novaLinha$Hora[j] == 0) {
    GynClima <- InsertRow(GynClima, NewRow = novaLinha[j, ], RowNum = n[j])
    n[-(1:(j-1))] <- n[-(1:(j-1))] + 1
  } else {
    GynClima <- InsertRow(GynClima, NewRow = novaLinha[j, ], RowNum = n[j] + 1)
    n[-(1:(j-1))] <- n[-(1:(j-1))] + 1
  }
}
n
# Uma vez identificadas essa 118 datas não repicladas, em que a medida de zero hora (4) ou de 12:00 h (114) não estão presentes decidiu-se tratar essa inconsistência pela inserção de NA's em todas as respectivas medidas não registradas
```
```{r TransformarDados3}
# É preciso verificar se há alguma data em que não houve o registro das duas observações do dia.
# E se isso se verifica para todas as 24.834 linhas de GynClima
library(tidyverse)
# (GynClima$Data != lead(GynClima$Data)) != (GynClima$Hora == 1200L)
# Testando se todoas as datas encontram-se registradas duas vezes, ou seja, se ela é igual sua sucessora e diferente da sucessora da sua sucessora.
if (is.na(GynClima[(GynClima$Data != lead(GynClima$Data)) != (GynClima$Hora == 1200L), ])) {
  print("Não há datas que não se encontram registradas duas vezes, sendo uma para 12:00 h")
  GynClima[(GynClima$Data != lead(GynClima$Data)) != (GynClima$Hora == 1200L), ]
} else {
  print("A seguir as datas que não se encontram registradas duas vezes:")
  GynClima[(GynClima$Data != lead(GynClima$Data)) != (GynClima$Hora == 1200L), ]
}
# Pode-se concluir que não há mais datas que não apresentem dois registros em cada dia: às 0 h e às 12:00 h 
```


```{r TransformarDados4}
# Ainda é preciso verificar se há alguma data em que não houve o registro das duas observações do dia.
# Isso para todas as 24.834 linhas de GynClima
# Ou seja, se a série histórica segue a sequência de todas as datas, dia após dia: 01/01/1985-31/12/2018 (34 anos)
library(tidyverse)
library(magrittr)
# (GynClima$Data != lead(GynClima$Data)) != (GynClima$Hora == 1200L)
# Testando se todoas as datas encontram-se registradas duas vezes, ou seja, se ela é igual sua sucessora e diferente da sucessora da sua sucessora.
seqDatasDuplicadas <- seq.Date(from = as.Date("1985-01-01"),
                               to   = as.Date("2018-12-31"),
                               by   = "day") %>% rep(each = 2)

GynClima[GynClima$Data != seqDatasDuplicadas, ]
# Indicou que faltaram as duas observações de 0 h e de 12:00 h do dia "1993-09-04"
nlinha <- row.names(GynClima[GynClima$Data != seqDatasDuplicadas, ][1, ]) %>%
          as.integer


faltaLinha <- data.frame(Estacao       = rep(GynClima$Estacao[1], each = 2),
                         Data          = rep(as.Date(GynClima[GynClima$Data != seqDatasDuplicadas, ][1, 2] - 1),
                                             each = 2),
                         Hora          = c(0L, 1200L),
                         Precipitacao  = rep(NA, each = 2),
                         TempMax       = rep(NA, each = 2),
                         TempMin       = rep(NA, each = 2),
                         TempCompMedia = rep(NA, each = 2)
                         )
row.names(faltaLinha) <- c(nlinha, nlinha + 1)

GynClima <- InsertRow(GynClima, NewRow = faltaLinha[1, ], RowNum = nlinha)
GynClima <- InsertRow(GynClima, NewRow = faltaLinha[2, ], RowNum = nlinha + 1)

# Falta testar se toda a sequencia de datas está completa e íntegra: 01/01/1985-31/12/2018 (34 anos)
length(GynClima$Data != seqDatasDuplicadas)
sum(GynClima$Data != seqDatasDuplicadas)
# Indicou que toda a sequencia de datas está completa e íntegra: 01/01/1985-31/12/2018 (34 anos)
# Pode-se concluir que não há mais datas que não apresentem dois registros em cada dia: às 0 h e às 12:00 h 
```


```{r TransformarDados5}
# Construindo um data frame adequado
# GynClima2 <- data.frame(
  # Data         = GynClima$Data[!duplicated(GynClima$Data)],
  # Precipitacao = GynClima$Precipitacao[!is.na(GynClima$Precipitacao)],
  # TempMin      = GynClima$TempMin[!is.na(GynClima$TempMin)],
  # TempMed      = GynClima$TempCompMedia[!is.na(GynClima$TempCompMedia)],
  # TempMax      = GynClima$TempMax[!is.na(GynClima$TempMax)]
# )
##########
# ERRO 2 #
##########
# Error in data.frame(Data = GynClima$Data[!duplicated(GynClima$Data)], : arguments imply differing number of rows: 12417, 12404, 12182, 12068, 12302
# Há uma inconcistência nos dados de entrada
# 12417 datas diferentes
# 12404 observações de precipitação => Há  13 dias sem registro desse dado (NA).
# 12182 observações de temp. Mínima => Há 235 dias sem registro desse dado (NA).
# 12068 observações de temp. Mínima => Há 349 dias sem registro desse dado (NA).
# 12182 observações de temp. Mínima => Há 235 dias sem registro desse dado (NA).
######################################################################
# É preciso colher esses dados sem perder essas informações e de modo a manter cada informação sincronizada com seu dia de coleta
# É preciso transformar essa lanilha de entrada num padrão data frame: 1 <var> por coluna
###################################################################### 1 <obs> por linha.

GynClima2 <- data.frame(Data         = GynClima$Data[!duplicated(GynClima$Data)],
                        Precipitacao = GynClima$Precipitacao [GynClima$Hora == 1200],
                        TempMin      = GynClima$TempMin      [GynClima$Hora == 1200],
                        TempMed      = GynClima$TempCompMedia[GynClima$Hora ==    0],
                        TempMax      = GynClima$TempMax      [GynClima$Hora ==    0]
)
# Simplificou-se o nome da <var> TempCompMedia, tipo ,dbl>, para TempMedia
# Já eliminou a <var> estação, que é um dado repetido pára todas as observações.

############
# SALVANDO #
############
# Salvando a Base de Dados depurada ou carregando ela da pasta BD:
code <- 1
if(code == 1) {
  save(GynClima2, file = "../BD/GynClima2.RData")
} else {
  # load(file = "../BD/GynClima2.RData", envir = .GlobalEnv)
}
```

```{r EstatisticaDescritivaGerarSerieTemporalRegularMultivariavel}
# Gerando uma Série Temporal ordenada dos Dados diários de Meteorologia de Gyn: 1985-2018
GynClima3 <- GynClima2 # obtendo uma cópia
rownames(GynClima3) <- GynClima3[, 1] # nominando as linhas com as datas dos dias.
nomes <- rownames(GynClima3)

# Criando uma série temporal (1985-2018 = 34 anos)
#              multivariável (Precipitacao, TempMaxima, TempMin, TempMedia)
# Que começa com e termina com Estados com nomes de Santos.
# txHomeMatiz <- matrix(as.numeric(txHomeMatiz), 21, 27)
# colnames(txHomeMatiz) <- nomes

library(stats)
GynClimats <- ts(as.matrix(GynClima3[, -1]), start = c(1985, 1), end = c(2018, 365), deltat = 1/365)
GynClimats # está gerando vários intervalos com NA's????????????????????????????????????????????????
plot(GynClimats)
plot(x = GynClima2[, "Data"], y = GynClima2[, "TempMed"])
plot(x = GynClima3[, "Data"], y = GynClima3[, "TempMed"])
hist(GynClima3[, "TempMed"])
hist(GynClima3[, "Precipitacao"])


## Create a daily Date object - helps my work on dates
inds <- seq(as.Date("1985-01-01"), as.Date("2018-12-31"), by = "day")

## Create a time series object
myts <- ts(as.matrix(GynClima3[, -1]),     # transform data frame in numeric matrix
           start = c(1985, as.numeric(format(inds[1], "%j"))),
           frequency = 365) # está gerando vários intervalos com NA's????????????????????????????????????????????????

library(forecast)
## use auto.arima to choose ARIMA terms
# fit <- auto.arima(myts) # Error in auto.arima(myts) : auto.arima can only handle univariate time series
## forecast for next 60 time points
# fore <- forecast(fit, h = 60)
## plot it
# plot(fore)

plot(myts)
```


## Solução Do Exercício N. 3

```{r GraficosSerieTemporal}
# São 34 anos de dados meteorológicos diários de Gyn: 1/1/1984 a 31/12/2018
library(graphics)
plot(myts)
plot(myts[, c("Precipitacao", "TempMed")])
plot(myts[, c("Precipitacao", "TempMin")])

# Um só gráfico com a série temporal de 2000 a 2018
plot(myts[, c("Precipitacao", "TempMed")], plot.type = "single", col = 1:2, xlim = c(2004, 2018))
legend("topright", legend = c("Precipitacao", "TempMed"), col = 1:2, lty = 1, cex = 0.6)

# Um só gráfico com a série temporal de 2017
plot(myts[, c("Precipitacao", "TempMed")], plot.type = "single", col = 1:2, xlim = c(2017, 2018),
     ylim = c(0, 80))
legend("topright", legend = c("Precipitacao", "TempMed"), col = 1:2, lty = 1, cex = 0.6)

# Um só gráfico com a série temporal de 2018
plot(myts[, c("Precipitacao", "TempMed")], plot.type = "single", col = 1:2, xlim = c(2018, 2019),
     ylim = c(0, 80))
legend("topright", legend = c("Precipitacao", "TempMed"), col = 1:2, lty = 1, cex = 0.6)

```


## Solução Do Exercício N. 4

```{r EstatisticasSerieTemporal}
# São 34 anos de dados meteorológicos diários de Gyn: 1/1/1984 a 31/12/2018
library(stats)
# temperatura média de Gyn em 2018
tempMedia <- mean(GynClima3$TempMed[GynClima3$Data >= as.Date("2018-01-01")], na.rm = TRUE)
tempMedia
DPtempMedia <- sd(GynClima3$TempMed[GynClima3$Data >= as.Date("2018-01-01")], na.rm = TRUE)
DPtempMedia

pluvMedia <- mean(GynClima3$Precipitacao[GynClima3$Data >= as.Date("2018-01-01")], na.rm = TRUE)
pluvMedia
DPpluvMedia <- sd(GynClima3$Precipitacao[GynClima3$Data >= as.Date("2018-01-01")], na.rm = TRUE)
DPpluvMedia

# Um só gráfico com a série temporal de 2018
plot(myts[, c("Precipitacao", "TempMed")], plot.type = "single", col = 1:2, xlim = c(2018, 2019),
     ylim = c(0, 80))
library(graphics)
abline(h = tempMedia, col = 2)
abline(h = c(tempMedia + DPtempMedia, tempMedia - DPtempMedia), col = 2, lty = 3)

abline(h = pluvMedia, col = 1)
abline(h = c(pluvMedia + DPpluvMedia, pluvMedia - DPpluvMedia), col = 1, lty = 3)

legend("topright", legend = c("Precipitacao", "TempMedia"), col = 1:2, lty = 1, cex = 0.6)


# Dois gráficos para a série temporal de 2018: Pluviometria e Temperatura Média
# par(mfrow = c(1, 2))
plot(myts[, c("Precipitacao")], col = 1, xlim = c(2018, 2019), ylim = c(0, 80))
abline(h = pluvMedia, col = 1)
abline(h = c(pluvMedia + DPpluvMedia, pluvMedia - DPpluvMedia), col = 1, lty = 3)
legend("topright", legend = c("Precipitacao"), col = 1, lty = 1, cex = 0.6)

plot(myts[, c("TempMed")], col = 2, xlim = c(2018, 2019), ylim = c(15, 30))
abline(h = tempMedia, col = 2)
abline(h = c(tempMedia + DPtempMedia, tempMedia - DPtempMedia), col = 2, lty = 3)
legend("topright", legend = c("TempMedia"), col = 2, lty = 1, cex = 0.6)
```



## Solução Do Exercício N. 5

```{r função_outliers, include = TRUE, echo = TRUE}
# Função p/retornar os outliers de um <vctr> <num> acima/abaixo de k * DP
# DP é o Desvio Padrão do vector x
# k será o número de Desvios Padrão acima e abaixo do que os outliers
# serão detectados
outliers <- function(x, k = 2, code = 0) {
  n  <- length(x)
  if(n == 0) return()
  if(n == 1) return(x)
  # Se code == -1 retornará os outliers abaixo de k * DP(x)
  # Se code ==  0 retornará os outliers abaixo ou acima de k * DP(x)
  # Se code == +1 retornará os outliers acima  de k * DP(x)
  m  <- mean(x, na.rm = TRUE)
  dp <-   sd(x, na.rm = TRUE)
  li <- m - k * dp
  ls <- m + k * dp
  if(code == -1) {
    # retorna c/o subconjunto dos outliers abaixo de k * DP(x)
    return(x[x <= li])
  }
  if(code ==  0) {
    # retorna c/o subconjunto dos outliers abaixo ou acima de k * DP(x)
    return(x[x <= li | x >= ls])
  }
  if(code == +1) {
    # retorna c/o subconjunto dos outliers acima de k * DP(x)
    return(x[x >= ls])
  }
  # para qualquer outro valor de code retorna com um vetor contendo
  # o intervalo (média - k * DP, média + k * DP)
  return(li, ls)
} # fim da função outliers
#-------------------------
```

```{r catNewLine, include = TRUE, echo = TRUE}
catNL <- function(...) {cat(paste(...) %+% "\n")}
# fim da função catNL
#--------------------
```

```{r newLine, include = TRUE, echo = TRUE}
newLine <- function() cat("\n")
# fim da função newLine
#----------------------
```

```{r pularnLinhasError, include = TRUE, echo = TRUE}
pularnLinhasError <- function(n = NA) {
  if(is.na(n)) stop(call. = FALSE)
  stopifnot(is.numeric(n))
  if(n < 1)    stop(call. = FALSE)
  else {
    newLine()
    pularnLinhasError(n - 1)
  } # fim do esle do if(n < 1)
} # fim da função pularnLinhasError
#----------------------------------
```

```{r pularnLinhas, include = TRUE, echo = TRUE}
pularnLinhas <- function(n) {
  try(pularnLinhasError(n), silent = TRUE)
} # fim da função pularnLinhas
#----------------------------
```


```{r OutliersSerieTemporal}
# São 34 anos de dados meteorológicos diários de Gyn: 1/1/1984 a 31/12/2018
# (k = 2; outliers)
# Resolvendo chamando uma função especializada
pularnLinhas(2)
catNL("Outliers k = 2 da Temperatura")
outliers(GynClima3$TempMed, k = 2, code = 0)# Não foram detectados outliers além de dois desvios padrões
catNL("Outliers k = 3 das Temperaturas")
outliers(GynClima3$TempMed, k = 3, code = 0) # Não foram detectados outliers além de três desvios padrões
pularnLinhas(2)
```


## Gerando Plots
  
Gráficos das temperaturas médias diárias e dos índices pluviométericos diários de Goiânia em 2018.  
  
```{r graficosDeTemperatruraEPluviometria}
# Dica: Faça um chunk para geração dos gráficos de cada exercício que exija essa forma de apresentação.


```
